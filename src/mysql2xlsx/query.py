# -*- coding: utf8 -*-

import os
import sys
import pymysql
from .workbook import Workbook
from .common import RESTRICTION_ROW_ON_SHEET, ExeededLimitCol, IllegalOperation


class Query(object):
    """Implementation of works
    with MySQL-procedures on remote host."""

    def __init__(self, query, output, host, db, port, user, password):

        self.query = str(query).strip()
        self.xlsxfile = output
        self.__select_xlsx_name()
        self.host = host
        self.db = db
        self.port = port
        self.user = user
        self.password = password
        self.wb = Workbook(name=self.xlsxfile)
        self.__result = list()
        self.__connecting()

    def __forbid_illegal_words(self):
        """Forbid query which contans illegal words without building syntax tree"""

        illegal_words = ['DELETE', 'TRUNCATE', 'DROP', 'INSERT', 'UPDATE']
        legal_words = ['SELECT', 'CALL', 'DO']
        if any([word in self.query.upper() for word in illegal_words]):
            raise IllegalOperation
        if not any([word in self.query.upper() for word in legal_words]):
            raise IllegalOperation

    def __select_xlsx_name(self):
        """We select the name if it is not specified."""
        if self.xlsxfile is None:
            self.xlsxfile = 'result.xlsx'
        if not self.xlsxfile.endswith('.xlsx'):
            self.xlsxfile = '%s.xlsx' % (self.xlsxfile)
        i = 1
        while True:
            if os.path.exists(os.path.join(os.getcwd(), self.xlsxfile)):
                self.xlsxfile = '%s(%s)%s' % (self.xlsxfile.split('.xlsx')[0].split('(')[0], i, '.xlsx')
                i += 1
            else:
                break

    def __connecting(self):
        """Connecting to database on the remote host."""

        try:
            self._connect = pymysql.connect(host=self.host,
                                            port=self.port,
                                            db=self.db,
                                            user=self.user,
                                            passwd=self.password
                                            )

            self._cursor = self._connect.cursor(pymysql.cursors.DictCursor)
            self.__coding()
        except pymysql.Error as e:
            sys.stderr.write('Error connection to remote host mysql: {}'.format(e))

    def __coding(self):
        self._cursor.execute('SET NAMES `utf8`')
        self._cursor.fetchall()

    def __reconnect(self):
        """Reconnect to mysql host."""
        more = True
        while more:
            try:
                self._cursor.fetchall()
            except AttributeError:
                # Disconnect to remote database.
                break
            # Do something with these results.
            more = self._cursor.nextset()
        try:
            self._cursor.close()
            self._cursor = self._connect.cursor(pymysql.cursors.DictCursor)
            self.__coding()
        except AttributeError:
            # Disconnect to remote database.
            pass

    def __execute(self):
        try:
            self.__forbid_illegal_words()
            self._cursor.execute(self.query)
        except AttributeError:
            # Disconnect to remote database.
            return
        except (pymysql.ProgrammingError, IllegalOperation) as e:
            sys.stderr.write('%s\n' % e)
            sys.exit(1)

        self.__result = self._cursor.fetchall()

    def __convert(self):
        """ """
        self.__execute()
        sheet, correction = 1, 0
        worksheet = self.wb.add_worksheet()
        self.len_result = len(self.__result)
        self.percent = self.len_result / 100.0
        try:
            header = self.__result[0].keys()
        except IndexError as e:
            sys.stderr.write('Result is emtpy!\n')
            sys.exit(1)

        worksheet.append_row(header)
        for row_number, row in enumerate(self.__result):
            try:
                values = [row.get(key) for key in header]
                worksheet.append_row(values)
            except ExeededLimitCol as e:
                sys.stderr.write(e.message)
                self.wb.clear()
                sys.exit(1)
            if row_number - correction >= RESTRICTION_ROW_ON_SHEET - 2:
                worksheet = self.ws.add_worksheet()
                correction = sheet * RESTRICTION_ROW_ON_SHEET
                sheet += 1
            yield row_number

    def convert(self):
        n = 0
        for i in self.__convert():
            k = i // self.percent
            if k != n:
                sys.stdout.write('\rMake xlsx: %s%%' % k)
                sys.stdout.flush()
            n == k
        sys.stdout.write('\rFile %s saved\n' % self.xlsxfile)
        self.wb.close()
