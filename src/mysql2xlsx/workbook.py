# -*- coding: utf8 -*-

import os
import shutil
from uuid import uuid4
from zipfile import ZipFile
from datetime import datetime
from tempfile import gettempdir

from .common import BASE_DIR
from .worksheet import Worksheet
from .sharedstring import sharedStrings


class Workbook(object):

    """The class provides fast entry in xlsx-flie."""

    def __init__(self, name, codepage=None):
        self.codepage = codepage
        self.worksheets = list()
        self.name = '%s' % name
        self.__create_path()
        self.shared = sharedStrings()

    def __create_path(self):
        self.__path = os.path.join(gettempdir(), uuid4().hex)
        shutil.copytree(os.path.join(BASE_DIR, 'mysql2xlsx/templates'), self.__path)

    def add_worksheet(self):
        """Its method added worksheet to workbook."""
        cn = len(self.worksheets) + 1
        if self.worksheets:
            self.worksheets[-1].save()
        self.worksheet = Worksheet(self.__path, cn, shared=self.shared, codepage=self.codepage)
        self.worksheets.append(self.worksheet)
        return self.worksheet

    def __gen_core_xml(self):
        """Generator of headers to xlsx."""
        with open(self.__path + '/docProps/core.tmpl', 'r') as core_tmpl:
            core_xml = core_tmpl.read().format(time=datetime.now().isoformat().split('.')[0] + 'Z')
        with open(self.__path + '/docProps/core.xml', 'w') as f:
            f.write(core_xml)

    def __gen_workbook_xml(self):
        """Generator workbook.xml."""
        with open(self.__path + '/xl/workbook.tmpl', 'r') as wb:
            tmpl = wb.read()
        for i, _ in enumerate(self.worksheets):
            tmpl += '<sheet name="Sheet{num}" sheetId="{num}" r:id="rId{num}"/>'.format(num=i + 1)
        tmpl += '</sheets><calcPr calcId="124519" fullCalcOnLoad="1"/></workbook>'

        with open(self.__path + '/xl/workbook.xml', 'w') as wb:
            wb.write(tmpl)

    def __gen_content_types(self):
        with open(self.__path + '/[Content_Types].tmpl', 'r') as wb:
            tmpl = wb.read()
        for i, _ in enumerate(self.worksheets):
            tmpl += '<Override PartName="/xl/worksheets/sheet{num}.xml"'.format(num=i + 1)
            tmpl += ' ContentType="application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml"/>'
        tmpl += '</Types>'
        with open(self.__path + '/[Content_Types].xml', 'w') as wb:
            wb.write(tmpl)

    def __gen_rels(self):
        with open(self.__path + '/xl/_rels/workbook.xml.rels.tmpl', 'r') as rels:
            tmpl = rels.read()
        len_worksheets = len(self.worksheets)
        for i in range(len_worksheets):
            tmpl += '<Relationship Id="rId%s"' % (i + 1)
            tmpl += ' Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet"'
            tmpl += ' Target="worksheets/sheet%s.xml"/>' % (i + 1)
        tmpl += '<Relationship Id="rId%s" ' % (len_worksheets + 1)
        tmpl += 'Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme" '
        tmpl += 'Target="theme/theme1.xml"/>'
        tmpl += '<Relationship Id="rId%s" ' % (len_worksheets + 2)
        tmpl += 'Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles" '
        tmpl += 'Target="styles.xml"/>'
        tmpl += '<Relationship Id="rId%s" ' % (len_worksheets + 3)
        tmpl += 'Type="http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings" '
        tmpl += 'Target="sharedStrings.xml"/>'
        tmpl += '</Relationships>'
        with open(self.__path + '/xl/_rels/workbook.xml.rels', 'w') as wb:
            wb.write(tmpl)

    def close(self):
        self.shared.save(path=self.__path)
        self.worksheets[-1].save()
        self.__gen_core_xml()
        self.__gen_workbook_xml()
        self.__gen_content_types()
        self.__gen_rels()
        found = []
        for root, dirs, files in os.walk(self.__path):
            found += [os.path.join(root, name) for name in files if not name.endswith('.tmpl')]

        with ZipFile(self.name, 'w', allowZip64=True) as zipfile:
            for i in found:
                name = i.split(self.__path)[1]
                zipfile.write(i, name)
        self.clear()

    def clear(self):
        shutil.rmtree(self.__path)
