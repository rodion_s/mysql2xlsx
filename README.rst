==========
mysql2xlsx
==========

Tool to make XSLX by means of direct query in MySQL.

If the rows is more than acceptable on a single sheet, adds a new sheet and fills it.

It compatible with Python version 2 and 3, also PyPy.

Installation:
=============
::

    $ sudo pip install git+https://bitbucket.org/rodion_s/mysql2xlsx


Usage:
======

::

   $ mysql2xlsx --host=127.0.0.1  --port=3306 --db=mysql --user=root --password=123 -q 'SELECT * FROM user' -o users.xlsx


License:
========

mysql2xlsx is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

mysql2xlsx is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with mysql2xlsx.  If not, see http://www.gnu.org/licenses/.
